<?php

if (!defined('ABSPATH')){
    exit;
}

function wpc_site_actions(){
	global $l, $error, $wp_config;

	$return = array();

	$request = wpc_optREQ('request');

	if(empty($request)){
		$return['error'] = $l['no_req_post'];
		echo json_encode($return);
		die();
	}
	
	if($request == 'update_website'){
		$source = urldecode(wpc_optREQ('source'));

		include_once(ABSPATH.'wp-admin/includes/class-wp-upgrader.php');
		include_once(ABSPATH.'wp-admin/includes/update.php');
		include_once(ABSPATH.'wp-admin/includes/misc.php');

		global $wp_filesystem;

		$upgrade_error = array();

		$wp_upgrader_skin = new WP_Upgrader_Skin();
		$wp_upgrader_skin->done_header = true;

		$wp_upgrader = new WP_Upgrader($wp_upgrader_skin);

		$res = $wp_upgrader->fs_connect(array(get_home_path(), WP_CONTENT_DIR));
		if (!$res || is_wp_error($res)){
			$upgrade_error[] = $res;
		}

		$download = $wp_upgrader->download_package($source);
		if (is_wp_error($download)){
			$upgrade_error[] = $download;
		}

		$working_dir = $wp_upgrader->unpack_package($download);
		if (is_wp_error($working_dir)){
			$upgrade_error[] = $working_dir;
		}

		$wp_dir = trailingslashit($wp_filesystem->abspath());

		if (!$wp_filesystem->copy($working_dir.'/wordpress/wp-admin/includes/update-core.php', $wp_dir.'wp-admin/includes/update-core.php', true)){
			$wp_filesystem->delete($working_dir, true);
			
			$upgrade_error[] = $l['copy_fail'];
		}

		$wp_filesystem->chmod($wp_dir.'wp-admin/includes/update-core.php', FS_CHMOD_FILE);
		include_once(get_home_path().'wp-admin/includes/update-core.php');

		if(!function_exists('update_core')){
			$upgrade_error[] = $l['call_update_fail'];
		}

		$result = update_core($working_dir, $wp_dir);
		if(is_wp_error($result)){
			$upgrade_error[] = $result->get_error_code();
		}

		if(!empty($upgrade_error)){
			$return['error'] = 'error: '.implode("\n", $upgrade_error);
		}
	}
		
	if(wpc_optGET('plugins') || wpc_optGET('plugin')){
		$plugins = urldecode($_REQUEST['plugins']);
		$arr_plugins = explode(',', $plugins);

		if($request == 'activate'){//Activate

			$res = wpc_activate_plugin($arr_plugins);
			if(!$res){
				$return['error'] = $l['err_activating_pl'];
			}        
		}elseif($request == 'deactivate'){//Deactivate

			$res = wpc_deactivate_plugin($arr_plugins);
			if(!$res){
				$return['error'] = $l['err_deactivating_pl'];
			}        
		}elseif($request == 'delete'){//Deactivate and then Delete

			$act_res = wpc_deactivate_plugin($arr_plugins);        
			if(!$act_res){
				$return['error'] = $l['err_deactivating_del_pl'];
			}
			
			$result = delete_plugins($arr_plugins);
			if(is_wp_error($result)) {
				$return['error'] = $result->get_error_message();
			}elseif($result === false) {
				$return['error'] = $l['err_deleting_pl'];
			}
		}elseif($request == 'install'){//Install Plugins
			
			$sources = urldecode($_REQUEST['sources']);
			$arr_sources = explode(',', $sources);
			
			$all_installed_plugins = array();
			
			foreach($arr_plugins as $plk => $plval){
				
				//Skip if the plugin is already installed
				if(wpc_is_plugin_installed($plval)){
					continue;
				}
				
				$filename = basename(parse_url($arr_sources[$plk], PHP_URL_PATH));

				$download_dest = $wp_config['uploads_dir'].'/'.$filename;
				$unzip_dest = $wp_config['plugins_root_dir'];

				wpc_get_web_file($arr_sources[$plk], $download_dest);

				if(wpc_sfile_exists($download_dest)){
					$res = wpc_unzip($download_dest, $unzip_dest);
				}

				@wpc_sunlink($download_dest);

				//Activate the installed plugin(s)
				$all_installed_plugins[] = wpc_get_plugin_path(ABSPATH.'wp-content/plugins/'.$plval, $plval);
			}
			
			//Activate the installed plugins
			wpc_activate_plugin($all_installed_plugins);

			if(!empty($error)){
				$return['error'] = $error;
			}
		}elseif($request == 'update'){
			
			$plugin_name = urldecode(wpc_optREQ('plugin'));
			$download_link = urldecode(wpc_optREQ('source'));
			$site_url = urldecode(wpc_optREQ('siteurl'));
			
			$filename = basename(parse_url($download_link, PHP_URL_PATH));
			
			$download_dest = $wp_config['uploads_dir'].'/'.$filename;
			$unzip_dest = $wp_config['plugins_root_dir'];
			
			wpc_get_web_file($download_link, $download_dest);
			
			if(wpc_sfile_exists($download_dest)){
				$res = wpc_unzip($download_dest, $unzip_dest);
			}
			
			@wpc_sunlink($download_dest);
			
			// Lets visit the installation once to make the changes in the database
			$resp = wp_remote_get($site_url);
			
			if(!empty($error)){
				$return['error'] = $error;
			}
		}
	}elseif(wpc_optGET('themes') || wpc_optGET('theme')){
		
		$themes = urldecode(wpc_optGET('themes'));
		$arr_themes = explode(',', $themes);

		$active_theme = array_keys(wpc_get_active_theme());

		//Do not activate/delete the theme if it is active
		foreach($arr_themes as $tk => $tv){
			if($active_theme[0] == $tv){
				unset($arr_themes[$tk]);
			}
		}
		
		if($request == 'activate' && count($arr_themes) == 1){//Activate
			
			$res = wpc_activate_theme($arr_themes);
			if(!empty($error)){
				$return['error'] = $error;
			}
			if(!$res){
				$return['error'] = $l['err_activating_theme'];
			}
			
		}elseif($request == 'delete'){//Delete
		
			$res = wpc_delete_theme($arr_themes);
			if(!empty($error)){
				$return['error'] = $error;
			}
			if(!$res){
				$return['error'] = $l['err_deleting_theme'];
			}
			
		}elseif($request == 'install'){//Install Themes
			
			$sources = urldecode($_REQUEST['sources']);
			$arr_sources = explode(',', $sources);
			
			foreach($arr_themes as $thk => $thval){
				
				//Skip if the theme is already installed
				if(wpc_is_theme_installed($thval)){
					continue;
				}
			
				$filename = basename(parse_url($arr_sources[$thk], PHP_URL_PATH));
				
				$download_dest = $wp_config['uploads_dir'].'/'.$filename;
				$unzip_dest = $wp_config['themes_root_dir'].'/';
				
				wpc_get_web_file($arr_sources[$thk], $download_dest);
				
				if(wpc_sfile_exists($download_dest)){
					$res = wpc_unzip($download_dest, $unzip_dest);
				}
				
				@wpc_sunlink($download_dest);
			}
			
			if(!empty($error)){
				$return['error'] = $error;
			}
		}elseif($request == 'update'){//Update Theme
			
			$theme_name = urldecode(wpc_optREQ('theme'));
			$download_link = urldecode(wpc_optREQ('source'));
			$site_url = urldecode(wpc_optREQ('siteurl'));
			
			$filename = basename(parse_url($download_link, PHP_URL_PATH));
			
			$download_dest = $wp_config['uploads_dir'].'/'.$filename;
			$unzip_dest = $wp_config['themes_root_dir'].'/';
			
			wpc_get_web_file($download_link, $download_dest);
			
			if(wpc_sfile_exists($download_dest)){
				$res = wpc_unzip($download_dest, $unzip_dest);
			}
			
			@wpc_sunlink($download_dest);
			
			// Lets visit the installation once to make the changes in the database
			$resp = wp_remote_get($site_url);
			
			if(!empty($error)){
				$return['error'] = $error;
			}
		}
	}

	if(empty($return['error'])){
		$return['result'] = 'done';
	}

	//Using serialize here as all_plugins contains class object which are not json_decoded in Softaculous.
	echo json_encode($return);

}